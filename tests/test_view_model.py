import pytest
from todo_app.data.view_model import ViewModel
from todo_app.data.item import Item

@pytest.fixture
def item_data_setup():
    items = []
    items.append(Item('12345', 'Sample Data 1', 'To Do'))
    items.append(Item('23456', 'Sample Data 2', 'Doing'))
    items.append(Item('34567', 'Sample Data 3', 'Done'))
    return ViewModel(items)

def test_todo_items_property_only_returns_items_with_status_todo(item_data_setup):
    # Arrange
    item_view_model = item_data_setup

    # Act
    todo_items_list = item_view_model.todo_items

    # Assert
    assert len(todo_items_list) == 1
    assert todo_items_list[0].status == 'To Do'
    assert todo_items_list[0].title == 'Sample Data 1'

def test_todo_items_property_only_returns_items_with_status_doing(item_data_setup):
    # Arrange
    item_view_model = item_data_setup
    # Act
    doing_items_list = item_view_model.doing_items

    # Assert
    assert len(doing_items_list) == 1
    assert doing_items_list[0].status == 'Doing'
    assert doing_items_list[0].title == 'Sample Data 2'

def test_todo_items_property_only_returns_items_with_status_done(item_data_setup):
    # Arrange
    item_view_model = item_data_setup
    # Act
    done_items_list = item_view_model.done_items

    # Assert
    assert len(done_items_list) == 1
    assert done_items_list[0].status == 'Done'
    assert done_items_list[0].title == 'Sample Data 3'