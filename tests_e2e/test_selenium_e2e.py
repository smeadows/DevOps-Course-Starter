import os
import pytest
import pymongo
from threading import Thread
from time import sleep
from selenium import webdriver
from dotenv import load_dotenv
from todo_app import app

@pytest.fixture(scope='module')
def app_with_temp_board():
    # Load our real environment variables
    load_dotenv(override=True)

    os.environ['LOGIN_DISABLED'] = 'True'
    client = pymongo.MongoClient(os.getenv('MONGODB_CONNECTION_STRING'))
    db = client['TestDB']
    collection = db['TestCollection']

    # Construct the new application
    application = app.create_app()

    # Start the app in its own thread.
    thread = Thread(target=lambda: application.run(use_reloader=False))
    thread.daemon = True
    thread.start()
    
    # Give the app a moment to start
    sleep(1)

    # Return the application object as the result of the fixture
    yield application

    # Tear down
    thread.join(1)
    client.drop_database('TestDB')

@pytest.fixture(scope='module')
def driver():
    opts = webdriver.ChromeOptions()
    opts.add_argument('--headless')
    opts.add_argument('--no-sandbox')
    opts.add_argument('--disable-dev-shm-usage')
    with webdriver.Chrome(options=opts) as driver:
        yield driver

def test_task_journey(driver, app_with_temp_board):
    driver.get('http://localhost:5000/')

    assert driver.title == 'To-Do App'