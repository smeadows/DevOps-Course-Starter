terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 3.8"
    }
  }
  backend "azurerm" {
        resource_group_name  = "Cohort23-24_SimMea_ProjectExercise"
        storage_account_name = "sgmterrastatestore"
        container_name       = "sgmterrastatecontainer"
        key                  = "terraform.tfstate"
    }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "main" {
  name     = "Cohort23-24_SimMea_ProjectExercise"
}

resource "azurerm_service_plan" "main" {
  name                = "terraformed-asp"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "main" {
  name                = "sgmeadows-terra"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id

  site_config {
    application_stack {
      docker_image     = "sgmeadows/todo-app"
      docker_image_tag = "latest"
    }
  }

  app_settings = {
    "DOCKER_REGISTRY_SERVER_URL" = "https://index.docker.io"
    "MONGODB_COLLECTION_NAME" = "todo_items"
    "MONGODB_CONNECTION_STRING" = azurerm_cosmosdb_account.main.connection_strings[0]
    "MONGODB_DB_NAME" = "NewTrelloDB"
    "SECRET_KEY" = "secret-key"
    "GITLAB_TODOAPP_CLIENT_ID" = var.client_id
    "GITLAB_TODOAPP_SECRET" = var.client_secret
    "LOG_LEVEL" = "ERROR"
    "LOGGLY_TOKEN" = var.loggly_token
  }
}

resource "azurerm_cosmosdb_account" "main" {
  name                = "sgm-cosmos-db-terra"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  offer_type          = "Standard"
  kind                = "MongoDB"
  mongo_server_version = "4.2"

  capabilities {
      name = "EnableServerless"
  }

  capabilities {
      name = "EnableMongo"
  }

  consistency_policy {
    consistency_level       = "BoundedStaleness"
    max_interval_in_seconds = 300
    max_staleness_prefix    = 100000
  }

  geo_location {
    location          = "westus"
    failover_priority = 0
  }
}

resource "azurerm_cosmosdb_mongo_database" "main" {
  name                = "sgm-cosmos-mongo-db-terra"
  resource_group_name = data.azurerm_resource_group.main.name
  account_name        = azurerm_cosmosdb_account.main.name

  lifecycle {
    prevent_destroy = true
  }
}