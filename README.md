# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

### Azure Cosmos DB Integration

The project integrates with an Azure Cosmos DB to store to-do tasks.

You will need to create an Azure CosmosDB database to go alongside your existing App Service.

Log into Azure and create a new CosmosDB instance in your resource group:

In the Azure Portal:

- New -> CosmosDB Database
- Select “Azure Cosmos DB API for MongoDB”
- Choose “Serverless” for Capacity mode

Once your CosmoDB is up and running you can copy the PRIMARY CONNECTION STRING for your CosmosDB cluster, available under Settings -> Connection String from your CosmosDB account page in the Azure portal.

This can then be added as an Application setting.


## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Testing

Pytest is used for testing purposes.

To run pytest (which will find and execute all defined tests), simply run the following command from the root of the project:

```bash
$ poetry run pytest tests/
```

To run a specific set of tests, execute one or more of the following commands:

```bash
$ poetry run pytest tests/test_app.py
$ poetry run pytest tests/test_view_model.py
```

The selenium driver is used for the e2e testing found under tests_e2e.

There are Selenium driver's for most major browsers, but the example below and included in the test_selenium_e2e.py uses ChromeDriver.

You will need to download the ChromeDriver executable and place it in the root of your project - the selenium driver just uses this under the hood.

The following code snippet is used to create a fixture for ChromeDriver:
```
from selenium import webdriver

@pytest.fixture(scope="module")
def driver():
    with webdriver.Chrome() as driver:
    yield driver
```

## VM Provisioning

In order to provision a ToDo App VM from an Ansible control node, run the following command:

```
$ ansible-playbook my-ansible-playbook.yml -i my-ansible-inventory
```

You will be prompted to enter a Trello API Key and Token during the playbook execution.

## Docker Build

In order to build and run the todo-app within a docker container, you'll first need to install [Docker Desktop](https://www.docker.com/products/docker-desktop/). Installation instructions for Windows can be found [here](https://docs.docker.com/desktop/install/windows-install/).

If prompted to choose between using Linux or Windows containers during setup, make sure you choose Linux containers.
### **Production Build**
Once installed and running, to build and run a production version, execute the following commands:

```
$ docker build --target production --tag todo-app:prod .
```
```
$ docker run -p 8000:8000 --env-file ./.env todo-app:prod
```
And then visit http://localhost:8000/ in your browser to see the site.
### **Development Build**

To build and run a development version, execute the following commands:
```
$ docker build --target development --tag todo-app:dev .
```
```
$ docker run -p 8000:5000 --env-file ./.env todo-app:dev
```
To run a development version with a locally bound version of your todo-app run the following command from the directory in which your todo_app folder resides:
```
$ docker run -p 8000:5000 --env-file ./.env --mount type=bind,source="$(pwd)",target=/todo_app_base todo-app:dev
```
With either run option, visit http://localhost:5000/ in your browser to see the site.
### **Testing Build**
To build and run a testing version, execute the following command:
```
$ docker build --target test --tag my-test-image .
```
To run the unit and integration tests, execute the following command:
```
$ docker run --env-file .env.test my-test-image tests
```
To run the end-2-end Selenium tests, execute the following command:
```
$ docker run --env-file .env my-test-image tests_e2e
```

### **Manual push of Production container image**
Docker Hub is used as the registry to store the production container images.

In order to manually move this, follow the steps below:

Login to DockerHub locally; --username and --password may not be required:

```
$ docker login --username <USERNAME> --password <PASSWORD>
```

Build the image:

```
$ docker build --target production --tag <USERNAME>/todo-app:prod .
```

Push the image:

```
$ docker push <USERNAME>/todo-app:prod
```

## **Azure Hosted Site**

Azure hosted site can be found at:

https://sgm-project-exercise.azurewebsites.net/

## **Authentication and Authorisation**

Only Authenticated users are able to access the ToDo App.

This is handled via GitLab.

2 authorisation roles have been created:

- reader
    - These users can view to-dos but not change or create new ones
- writer
    - These users can also create new to-dos and update or delete existing ones

This is currently hard-coded into the app so the current user has the writer role.

## **Running locally via Minikube**

Minikube is a version of Kubernetes that you can run locally on a development machine.

The following tools need to be installed and running (when appropriate) on your local machine:

- Docker
    - [Docker Desktop (Windows)](https://docs.docker.com/desktop/install/windows-install/)
    - [Docker Desktop (Mac)](https://docs.docker.com/desktop/install/mac-install/)
    - [Docker Engine (Linux)](https://docs.docker.com/engine/install/#server)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)

Run the following command in an admin terminal to spin up your minikube cluster:
```
$ minikube start
```
Create a Docker image for the Pod by navigating to the folder containing the todo-app run the following commond:
````
$ run docker build --target production --tag todo-app:prod .
````
Run the following command to load it into minikube’s local image store:
````
$ minikube image load todo-app:prod
````
Run the following command to create your secrets file, substituting in the appropriate values:
````
$ kubectl create secret generic my-secrets
    --from-literal=SECRET_KEY='<SECRET_KEY>'
    --from-literal=MONGODB_CONNECTION_STRING='<MONGODB_CONNECTION_STRING>'
    --from-literal=LOGGLY_TOKEN='<LOGGLY_TOKEN>'
    --from-literal=GITLAB_TODOAPP_CLIENT_ID='<GITLAB_TODOAPP_CLIENT_ID>'
    --from-literal=GITLAB_TODOAPP_SECRET='<GITLAB_TODOAPP_SECRET>'
````

Run the following command to deploy a Pod running the todo-app image:
````
$ kubectl apply -f deployment.yaml
````

Run the following command to deploy the Service:
````
kubectl apply -f service.yaml
````
Run the following command to link up your minikube Service with a port on localhost:
````
$ kubectl port-forward service/module-14 7080:8000
````
Open http://localhost:7080/ in a browser to view the app.