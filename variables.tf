variable "client_id" {
    description = "GITLAB_TODOAPP_CLIENT_ID"
    type = string
    sensitive = true
}

variable "client_secret" {
    description = "GITLAB_TODOAPP_SECRET"
    type = string
    sensitive = true
}

variable "loggly_token" {
    description = "LOGGLY_TOKEN"
    type = string
    sensitive = true
}