from flask import Flask, render_template, redirect, request, session
from todo_app.data.todo_items_functions import get_todo_cards, delete_todo_card, add_todo_card, mark_todo_card_todo_doing_done
from todo_app.flask_config import Config
from todo_app.data.view_model import ViewModel
from flask_login import LoginManager, login_required, UserMixin, login_user, current_user
from functools import wraps
from loggly.handlers import HTTPSHandler
from logging import Formatter
import os
import random
import string
import requests
import jwt

def create_app():
    class User(UserMixin):
        def __init__(self, id):
            self.id = id
            if self.id == '782264':
                self.role = 'writer'
            else:
                self.role = 'reader'

    app = Flask(__name__)
    app.config.from_object(Config())
    app.config['LOGIN_DISABLED'] = os.getenv('LOGIN_DISABLED') == 'True'
    app.config['LOG_LEVEL'] = os.getenv('LOG_LEVEL')
    app.config['LOGGLY_TOKEN'] = os.getenv('LOGGLY_TOKEN')

    app.logger.setLevel(app.config['LOG_LEVEL'])

    if app.config['LOGGLY_TOKEN'] is not None:
        handler = HTTPSHandler(f'https://logs-01.loggly.com/inputs/{app.config["LOGGLY_TOKEN"]}/tag/todo-app')
        handler.setFormatter(
            Formatter("[%(asctime)s] %(levelname)s in %(module)s: %(message)s")
        )
        app.logger.addHandler(handler)

    def generate_random_string():
        letters = string.ascii_lowercase
        random_string = ''.join(random.choice(letters) for i in range(20))
        return random_string

    login_manager = LoginManager()

    @login_manager.unauthorized_handler
    def unauthenticated():
        app.logger.info("User not authenticated; authenticating")
        client_id = os.environ.get('GITLAB_TODOAPP_CLIENT_ID')
        redirect_uri = f'{request.host_url}login/callback'
        session['state'] = generate_random_string()

        authentication_url = 'https://gitlab.com/oauth/authorize?client_id=' + client_id + '&redirect_uri=' + redirect_uri + '&response_type=code&state=' + session['state'] + '&scope=openid'

        return redirect(authentication_url)

    @login_manager.user_loader
    def load_user(user_id):
        return User(user_id)
 
    login_manager.init_app(app)

    @app.route('/login/callback')
    def callback_route():
        auth_code = request.args.get('code', '')
        returned_state = request.args.get('state', '')

        if (returned_state != session['state']):
            app.logger.error("Returned state does not match session state")
            return
        
        client_id = os.environ.get('GITLAB_TODOAPP_CLIENT_ID')
        client_secret = os.environ.get('GITLAB_TODOAPP_SECRET')
        redirect_uri = f'{request.host_url}login/callback'

        app.logger.info("Authorising user")

        parameters = 'client_id=' + client_id + '&client_secret=' + client_secret + '&code=' + auth_code +'&grant_type=authorization_code&redirect_uri=' + redirect_uri

        url = 'https://gitlab.com/oauth/token'

        headers = {
            "Accept": "application/json"
        }

        response = requests.request(
            "POST",
            url,
            headers=headers,
            params=parameters
        )

        app.logger.info("Response: %s", response)

        if (response.status_code != 200):
            app.logger.error("Error authenticating. HTTP response %s returned.", response.status_code)
            return 'Error'

        returned_id_token = response.json()['id_token']
        decoded_user_id = jwt.decode(returned_id_token, options={"verify_signature": False})['sub']
        authd_user = User(decoded_user_id)

        login_user(authd_user)

        return redirect('/')

    def can_user_write():
        if app.config['LOGIN_DISABLED']:
            return True
        elif current_user.role == 'writer':
            return True
        else:
            return False
        
    def write_access_required(func):
        @wraps(func)
        def check_role(*args, **kwargs):
            if can_user_write():
                return func(*args, **kwargs)
            else:
                return redirect('/')
        return check_role

    def write_access_required(func):
        @wraps(func)
        def check_role(*args, **kwargs):
            if can_user_write():
                return func(*args, **kwargs)
            else:
                return redirect('/')
        return check_role

    @app.route('/')
    @login_required
    def index():
        items = get_todo_cards()

        app.logger.info("ToDo Cards returned: %s", items)

        item_view_model = ViewModel(items)

        return render_template('index.html', view_model=item_view_model, writer_role=can_user_write())

    @app.route('/add', methods=['POST'])
    @login_required
    @write_access_required
    def add_item_to_list():
        title = request.form.get('to_do_title')
        add_todo_card(title)
        
        app.logger.info("Added ToDo item")

        return redirect('/')

    @app.route('/marktodo/<id>', methods=['POST'])
    @login_required
    @write_access_required
    def mark_item_todo(id):
        mark_todo_card_todo_doing_done(id, 'To Do')

        app.logger.info("ToDo item marked as To Do")

        return redirect('/')

    @app.route('/markdoing/<id>', methods=['POST'])
    @login_required
    @write_access_required
    def mark_item_doing(id):
        mark_todo_card_todo_doing_done(id, 'Doing')

        app.logger.info("ToDo item marked as Doing")

        return redirect('/')

    @app.route('/markdone/<id>', methods=['POST'])
    @login_required
    @write_access_required
    def mark_item_done(id):
        mark_todo_card_todo_doing_done(id, 'Done')

        app.logger.info("ToDo item marked as Done")

        return redirect('/')

    @app.route('/delete/<id>', methods=['POST'])
    @login_required
    @write_access_required
    def delete_item_from_list(id):
        delete_todo_card(id)

        app.logger.info("ToDo item Deleted")

        return redirect('/')

    return app