from todo_app.data.item import Item
from typing import List


class ViewModel:
    def __init__(self, items: List[Item]):
        self._items = items

    @property
    def items(self):
        return self._items

    @property
    def todo_items(self):
        just_todo_items = list(filter(lambda x: (x.status == 'To Do'), self._items))
        return just_todo_items

    @property
    def doing_items(self):
        just_doing_items = list(filter(lambda x: (x.status == 'Doing'), self._items))
        return just_doing_items


    @property
    def done_items(self):
        just_done_items = list(filter(lambda x: (x.status == 'Done'), self._items))
        return just_done_items