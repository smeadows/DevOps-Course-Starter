import os
from todo_app.data.item import Item
import pymongo

def get_collection():
    client = pymongo.MongoClient(os.getenv('MONGODB_CONNECTION_STRING'))
    db = client[os.getenv('MONGODB_DB_NAME')]
    collection = db[os.getenv('MONGODB_COLLECTION_NAME')]
    return collection

def get_todo_cards():
    item_list = []

    for list_obj in get_collection().find():
        item = Item.from_todo_card(list_obj)
        item_list.append(item)

    return item_list

def add_todo_card(title):
    item_to_add = {
        'name': title,
        'item_status': 'To Do'
    }

    get_collection().insert_one(item_to_add)

def delete_todo_card(card_id):
    get_collection().delete_one({"_id" : card_id})

def mark_todo_card_todo_doing_done(card_id, todo_doing_done):
    get_collection().update_one({"_id" : card_id}, { "$set": {"item_status": todo_doing_done}})