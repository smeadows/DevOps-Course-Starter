class Item:
    def __init__(self, id, title, status):
        self.id = id
        self.title = title
        self.status = status
 
    @classmethod
    def from_todo_card(cls, card):
        return cls(card['_id'], card['name'], card['item_status'])